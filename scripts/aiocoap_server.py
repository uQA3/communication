#! /usr/bin/python3
import datetime
import logging
import argparse

import asyncio

from influxdb import InfluxDBClient
import aiocoap.resource as resource
import aiocoap
import os

influxdb_client = None

def typetovalue(t):
    types = {
        0: 'imu_telemtry',
        1: 'heartbeat',
        2: 'motors_telemetry',
        3: 'pid_telemetry',
        4: 'kalman_telemetry',
        5: 'height_telemetry',
        }
    return types[t]

class TelemetryHandler(resource.Resource):

    i = 0
    json_body = []
    def __init__(self):
        super().__init__()

    async def render_post(self, request):
        p = eval(request.payload)
        values = p['data']

        for key in values:
            values[key] = float(values[key])

        if influxdb_client:
            self.json_body.append(
                {
                    "measurement": typetovalue(p['type']),
                    "tags": {
                        "uav": p['uav'],
                    },
                    "time": datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                    "fields": values,
                }
            )
            self.i += 1;
            if (self.i % 10 == 0):
                self.i = 0
                influxdb_client.write_points(self.json_body)
                self.json_body = []
        print(values)
      
        return aiocoap.Message(code=aiocoap.CHANGED, payload=b"ok")

# logging setup

logging.basicConfig(level=logging.INFO)
logging.getLogger("coap-server").setLevel(logging.INFO)

def main(args):
    global influxdb_client

    if args.influxdb_host:
        influxdb_client = InfluxDBClient(args.influxdb_host, args.influxdb_port, 'root', 'root', args.influxdb_table)

    root = resource.Site()

    root.add_resource(('other',), TelemetryHandler())

    asyncio.Task(aiocoap.Context.create_server_context(root))
    asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
    p = argparse.ArgumentParser()

    p.add_argument('--influxdb-host', type=str, help="InfluxDB IP")
    p.add_argument('--influxdb-port', type=int, help="InfluxDB Port")
    p.add_argument('--influxdb-table', type=str, help="InfluxDB Table")
    args = p.parse_args()
 
    main(args)
