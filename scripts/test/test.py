from __future__ import division
import socket, select, sys
import time

TCP_IP = ''
TCP_PORT = 

BUFFER_SIZE = 1024
#MESSAGE = "Hello, Server. Are you ready?\n"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
#s.send(MESSAGE)
socket_list = [sys.stdin, s]

while 1:
    read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [])

    for sock in read_sockets:
        # incoming message from remote server
        if sock == s:
            data = sock.recv(4096)
            if not data:
                print('\nDisconnected from server')
                sys.exit()
            else:
                sys.stdout.write("\n")
                message = data.encode('ascii')
                sys.stdout.write("\nMessage: " + str(message))
                sys.stdout.write('\n<Me> ')
                sys.stdout.flush()

        else:
            msg = sys.stdin.readline()
            print(time.time())
            s.send(bytes(msg))
            sys.stdout.write('<Me> ')
            sys.stdout.flush()
